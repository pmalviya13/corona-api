from django.contrib import admin
from django.urls import path, include
# from rest_framework import routers
from rest_framework.routers import DefaultRouter

from api.patient import views
from rest_framework_extensions.routers import NestedRouterMixin
from rest_framework_extensions.routers import ExtendedSimpleRouter
router = ExtendedSimpleRouter()
(
    router.register('', views.PatientViewset, basename='patient')
          .register(r'observations',
                    views.ObservationViewset,
                    basename='patient-observations',
                    parents_query_lookups=['patient'])
          .register(r'comments',
                    views.Commentviewset,
                    basename='observation-comments',
                    parents_query_lookups=['observation__patient','observation'])

)

urlpatterns = [
    path('',include(router.urls)),

]