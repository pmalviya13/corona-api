from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

import os


User = get_user_model()


class Command(BaseCommand):
    help = 'Creates superuser'

    def handle(self, *args, **options):
        try:
            user = User.objects.create_superuser(email='admin@gmail.com', password='Pp@123123')
            self.stdout.write(self.style.SUCCESS('Successfully created superuser\n email - admin@gmail.com\n password - Pp@123123'))

        except:
            self.stdout.write(self.style.ERROR('Unable to create superuser'))
