from django.contrib import admin

# Register your models here.
from api.patient.models import Patient, Observation,Comment

admin.site.register(Patient)
admin.site.register(Observation)
admin.site.register(Comment)