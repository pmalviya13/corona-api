from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# ALLOWED_HOSTS = ['localhost','127.0.0.1']

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# for AVL network
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'pmalviya13@gmail.com'
EMAIL_HOST_PASSWORD = 'ldctsmkautcqygkp'
EMAIL_USE_TLS = True


# INSTALLED_APPS += ["gunicorn"]


# MIDDLEWARE += [
#     # Simplified static file serving.
#     # https://warehouse.python.org/project/whitenoise/
#     "whitenoise.middleware.WhiteNoiseMiddleware"
# ]

# Static files config
# STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

# # SSL: https://docs.djangoproject.com/en/dev/topics/security/#ssl-https
# SECURE_SSL_REDIRECT = True
# SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True

