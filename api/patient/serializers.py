from django.contrib.auth import get_user_model
from rest_framework import serializers

from api.account.serializers import UserSerializer3
from api.patient.models import Patient, Observation, Comment

User=get_user_model()

class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('id','email','mobile_number',)

class PatientSerializer(serializers.ModelSerializer):
    registered_by = DoctorSerializer(read_only=True)
    class Meta:
        model=Patient
        fields = ('id','name','address',
                  'date_of_birth','mobile_number','aadhar_number',
                  'registered_by','gender','created_at',)
        read_only_fields = ('registered_by','created_at',)




class ObservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Observation
        fields = ('id','patient','observed_by','observed_at',
                  'heart_rate','o2_saturation',
                  'sob','dizziness','reduced_urine',
                  'respiratory_rate','chest_pain',
                  'unconscious','confusion',
                  'cold_extremities','fever','nasal',
                  'vomiting','anosmia','continuous_cough',
                  'body_aches','sore_throat','headache',
                  'body_temperature','rt_pcr_test','existing_illness',
                  'file',
                  'mews_value','ggo','probability','result',
                  )
        read_only_fields = ('observed_by','observed_at','patient','mews_value','ggo','probability','result',)



class CommentSerializer(serializers.ModelSerializer):
    comment_by = UserSerializer3(read_only=True)
    class Meta:
        model=Comment
        fields = ('id','comment','comment_by','created_at',)