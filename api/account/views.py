from django.contrib.auth import get_user_model
from django.shortcuts import render

# Create your views here.
from rest_framework import mixins, viewsets, generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.account.exceptions import InvalidToken
from api.account.models import TokenVerification
from api.account.serializers import UserCreateSerializer, TokenVerificationSerializer, ResendEmailSerializer, \
    UserReadSerializer
from api.core.utils import is_valid_uuid

User = get_user_model()


class UserViewset(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    name = 'user-create'
    queryset = User.objects.all()


    def get_serializer_class(self):
        if self.request.method == 'POST':
            return UserCreateSerializer
        else:
            return UserReadSerializer

class TokenViewset(generics.RetrieveAPIView):
    queryset = TokenVerification.objects.all()
    serializer_class = TokenVerificationSerializer


class ResendEmail(generics.CreateAPIView):
    name='resend-email'
    serializer_class = ResendEmailSerializer


class VerifyToken(APIView):
    name = 'verify-token'
    def get(self, request, token=None):
        print('asd', self.kwargs)
        token = self.kwargs['token']
        if not is_valid_uuid(token):
            raise InvalidToken

        try:
            token = TokenVerification.objects.get(token=token)
            user = token.user
            user.is_active = True
            user.save()
            token.delete()
            return Response({"detail": "Token Verified"}, status.HTTP_200_OK)
        except TokenVerification.DoesNotExist:
            raise InvalidToken


