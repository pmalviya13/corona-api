import random

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

import os

from api.patient.models import Patient

User = get_user_model()
from faker import Faker
fake = Faker()

class Command(BaseCommand):
    help = 'Creates superuser'

    def handle(self, *args, **options):

        users = User.objects.all()

        for i in range(1000):
            print(i)
            u=users[random.randint(0, 7)]

            if i % 2 == 0:

                Patient.objects.create(
                    name=fake.name_female(),
                    address = fake.address(),
                    phone_number = '91'+str(int(random.random()*(10**10))),
                    date_of_birth=fake.date_of_birth(),
                    gender='F',
                    aadhar_number=int(random.random()*(10**12)),
                    registered_by=u,
                    created_at = fake.date_time()
                )
            else:
                Patient.objects.create(
                    name=fake.name_male(),
                    address=fake.address(),
                    phone_number='91'+str(int(random.random()*(10**10))),
                    date_of_birth=fake.date_of_birth(),
                    gender='M',
                    aadhar_number=int(random.random() * (10 ** 12)),
                    registered_by=u,
                    created_at=fake.date_time()
                )
        try:
            user = User.objects.create_superuser(email='admin@gmail.com', password='Pp@123123')
            self.stdout.write(self.style.SUCCESS('Successfully created superuser\n email - admin@gmail.com\n password - Pp@123123'))

        except:
            self.stdout.write(self.style.ERROR('Unable to create superuser'))
