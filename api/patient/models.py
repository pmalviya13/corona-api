from django.conf import settings
from django.db import models

# Create your models here.
from api.patient.choices import GENDER_CHOICES, RP_PCR_CHOICES, VOMITING_CHOICES, MEWS_VALUE_CHOICES


class Patient(models.Model):

    name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    mobile_number = models.CharField(max_length=15,blank=True, default='')
    aadhar_number = models.CharField(max_length=12,blank=True, default='')
    address = models.TextField(blank=True, default='')
    registered_by = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    gender = models.CharField(choices=GENDER_CHOICES,max_length=1)

    def __str__(self):
        return self.name

class Observation(models.Model):
    def get_upload_path(instance, filename):
        p = 'patient_{0}/observations/{1}'.format(instance.patient.id,filename)
        return p

    patient = models.ForeignKey(Patient,on_delete=models.CASCADE,related_name='observations')
    observed_by = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.DO_NOTHING,related_name='observations')
    observed_at = models.DateTimeField(auto_now_add=True)

    heart_rate = models.IntegerField(null=True,blank=True)
    o2_saturation = models.FloatField(null=True,blank=True)
    sob = models.BooleanField()
    dizziness =models.BooleanField()
    reduced_urine = models.BooleanField()
    respiratory_rate = models.IntegerField(null=True,blank=True)
    chest_pain = models.BooleanField()
    unconscious = models.BooleanField()
    confusion =models.BooleanField()
    cold_extremities =models.BooleanField()
    fever = models.BooleanField()
    nasal = models.BooleanField()
    vomiting = models.CharField(choices=VOMITING_CHOICES,max_length=10)
    anosmia = models.BooleanField()
    continuous_cough = models.BooleanField()
    body_aches = models.BooleanField()
    sore_throat = models.BooleanField()
    headache = models.BooleanField()
    body_temperature = models.FloatField(help_text='body temperature in fahrenheit',null=True,blank=True)
    rt_pcr_test = models.CharField(choices=RP_PCR_CHOICES,max_length=10)
    existing_illness = models.TextField(default='')
    file = models.FileField(upload_to=get_upload_path,max_length=10000000000000000,null=True,blank=True)

    probability = models.FloatField(null=True, blank=True)
    result = models.CharField(max_length=10, choices=MEWS_VALUE_CHOICES, default=MEWS_VALUE_CHOICES[0][0])
    mews_value = models.IntegerField(null=True,blank=True)
    ggo = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.patient.name

class Comment(models.Model):
    comment = models.TextField(max_length=3000)
    observation = models.ForeignKey(Observation,on_delete=models.CASCADE,related_name='comments')
    created_at = models.DateTimeField(auto_now_add=True)
    comment_by = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.DO_NOTHING,related_name='observation_comments')
