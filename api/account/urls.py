from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from api.account import views

router = routers.DefaultRouter()
router.register('',views.UserViewset,basename=views.UserViewset.name)
urlpatterns = [
    path('',include(router.urls)),
    path('resend-email/',views.ResendEmail.as_view(),name=views.ResendEmail.name),
    path('verify-token/<token>',views.VerifyToken.as_view(),name=views.VerifyToken.name)
]