import logging
import uuid

from django.conf import settings
from django.core.mail import EmailMultiAlternatives

logger = logging.getLogger(__name__)

def is_valid_uuid(val):
    try:
        uuid.UUID(str(val))
        return True
    except ValueError:
        return False

def send_email(subject, to: list, content, h_content=None):
    from_email = settings.EMAIL_HOST_USER

    msg = EmailMultiAlternatives(subject, content, from_email, to)
    if h_content is not None:
        msg.attach_alternative(h_content, "text/html")
    msg.send()
    logger.info('Email successfully sent to {0}'.format(to))