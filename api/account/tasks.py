import logging

from django.conf import settings

from api.core.utils import send_email

logger = logging.getLogger(__name__)


def send_register_mail(user,token):
    """send email """
    logger.info('SENDING EMAIL')
    html_content = '<p>Hi, <strong>{0}</strong></p><br>Please verify your email address for the  Account. Just click on the <a href="http://{1}:{2}/verify-email/{3}">link</a>.</p><p>Best Regards,<br>Corona Team<br></p>'.format(
        user.first_name,settings.DOMAIN,settings.PORT,token.token
    )

    return send_email('test email', [user.email], 'asd', html_content)
