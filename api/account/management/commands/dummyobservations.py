import random

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

import os

from api.patient.choices import VOMITING_CHOICES, RP_PCR_CHOICES
from api.patient.models import Patient, Observation

User = get_user_model()
from faker import Faker
fake = Faker()


def get_random_bool():
    r = random.randint(0, 1)
    if r == 1:
        return True
    else:
        return False

class Command(BaseCommand):
    help = 'Creates superuser'

    def get_random_patient(self):
        return random.randint(1, 98)



    def handle(self, *args, **options):

        users = User.objects.all()
        patients=Patient.objects.all()[1:100]

        for i in range(10000):
            print(i)
            u=users[random.randint(0, 7)]

            Observation.objects.create(
                patient=patients[self.get_random_patient()],
                observed_by= users[random.randint(0, 7)],
                heart_rate=random.randint(50, 110),
                o2_saturation=random.randint(5,30 ),
                sob= get_random_bool(),
                dizziness= get_random_bool(),
                reduced_urine= get_random_bool(),
                respiratory_rate= random.randint(50,70 ),
                chest_pain= get_random_bool(),
                unconscious= get_random_bool(),
                confusion= get_random_bool(),
                cold_extremities= get_random_bool(),
                fever= get_random_bool(),
                nasal=get_random_bool(),
                vomiting= VOMITING_CHOICES[random.randint(0,3)][0],
                anosmia= get_random_bool(),
                continuous_cough= get_random_bool(),
                body_aches= get_random_bool(),
                sore_throat= get_random_bool(),
                headache= get_random_bool(),
                body_temperature= random.randint(30, 110),
                rt_pcr_test= RP_PCR_CHOICES[random.randint(0,2)][0],
                existing_illness= fake.sentence(),
            )
