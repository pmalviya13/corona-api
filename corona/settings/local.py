from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# ALLOWED_HOSTS = ['localhost','127.0.0.1']


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
# https://docs.djangoproject.com/en/dev/ref/settings/#internal-ips
INTERNAL_IPS = ["127.0.0.1",'localhost']

INSTALLED_APPS += ["debug_toolbar"]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]


# FOR MAILHOG
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = '' #xtract@1234
EMAIL_HOST = "localhost"
EMAIL_PORT = 1025
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"


#GMAIL CREDENTIALS
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = 'xtract.test001@gmail.com'
# EMAIL_HOST_PASSWORD = 'xtract@1234'
# EMAIL_USE_TLS = True

#set DJANGO_SETTINGS_MODULE=corona.settings.local