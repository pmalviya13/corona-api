import django_filters
import rest_framework
from django.contrib.auth import get_user_model
from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, filters as rest_filters, generics
from rest_framework.decorators import action
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework_extensions.mixins import NestedViewSetMixin

from api.patient.models import Patient, Observation, Comment
from api.patient.serializers import PatientSerializer, ObservationSerializer, CommentSerializer

User=get_user_model()
from django_filters import rest_framework as filters
class PatientFilter(filters.FilterSet):
    name = filters.CharFilter(field_name="name", lookup_expr='startswith')
    mobile_number = filters.CharFilter(field_name="mobile_number", lookup_expr='startswith')
    aadhar_number = filters.CharFilter(field_name="aadhar_number", lookup_expr='startswith')
    date_of_birth = filters.DateFilter(field_name="date_of_birth", lookup_expr='exact')

    class Meta:
        model = Patient
        fields = ['name','aadhar_number','mobile_number','date_of_birth']

class PatientViewset(NestedViewSetMixin, viewsets.ModelViewSet):
    name = 'patient-viewset'
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend,rest_framework.filters.OrderingFilter]

    ordering = ['-created_at']
    ordering_fields = ['created_at']

    pagination_class = LimitOffsetPagination
    filterset_class = PatientFilter

    def perform_create(self, serializer):
        serializer.save(registered_by=self.request.user)


class ObservationViewset(NestedViewSetMixin, viewsets.ModelViewSet):
    name = 'observation-viewset'
    queryset = Observation.objects.all()
    serializer_class = ObservationSerializer
    parser_classes = [FormParser, MultiPartParser]
    permission_classes = (IsAuthenticatedOrReadOnly,)
    pagination_class = LimitOffsetPagination
    filter_backends = [rest_framework.filters.OrderingFilter]
    ordering = ['-observed_at']
    ordering_fields = ['observed_at']
    def perform_create(self, serializer):
        serializer.save(observed_by=self.request.user,patient_id=self.kwargs['parent_lookup_patient'])


class Commentviewset(NestedViewSetMixin,viewsets.ModelViewSet):
    queryset = Comment.objects.all().prefetch_related('comment_by')
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    pagination_class = LimitOffsetPagination
    filter_backends = [rest_framework.filters.OrderingFilter]
    ordering = ['created_at']
    ordering_fields = ['created_at']
    def perform_create(self, serializer):
        serializer.save(comment_by=self.request.user,observation_id=self.kwargs['parent_lookup_observation'])

