from rest_framework.exceptions import APIException


class UserNotFound(APIException):
    status_code = 404
    default_detail = 'User not found'
class UserNotVerified(APIException):
    status_code = 400
    default_detail = 'Verification email has been resent, Please verify to continue'

class UserVerified(APIException):
    status_code = 400
    default_detail = 'user already verified, please login to continue'
class InvalidToken(APIException):
    status_code = 400
    default_detail = 'Token is not valid.'