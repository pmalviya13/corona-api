RP_PCR_CHOICES = (
        ('POS','Positive'),
        ('NEG','Negative'),
        ('NONE','Not Performed')
    )


GENDER_CHOICES = (
        ('M','Male'),
        ('F','Female'),
        ('O', 'Other')
    )


VOMITING_CHOICES = (
    ('NONE','None'),
    ('VOMITING','Vomiting'),
    ('DIARROEA','Diarroea'),
    ('BOTH','Both')
)

MEWS_VALUE_CHOICES = (
    ('0','No covid infection seen'),
    ('1','Stage 1 of Covid detected'),
    ('2A','Stage 2A of Covid detected'),
    ('3A','Stage 3A of Covid detected')
)