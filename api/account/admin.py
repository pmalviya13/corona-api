from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext as _

# Register your models here.
User = get_user_model()
class CustomUserAdmin(UserAdmin):
    """
    ordering is a variable which takes input list for ordering the column
    list_display is a variable which takes a list of columns to list to UI
    fieldsets define sections in our change and create page
    """
    ordering = ['id']
    list_display = ['email', 'mobile_number']
    readonly_fields = ['date_joined','last_login']
    fieldsets = (
        (None, {'fields':('email','password')}),
        (_('personal_info'), {'fields':('first_name','last_name','mobile_number',)}),
        (
            _("permissions"),
            {
                'fields' : ('is_active', 'is_staff', 'is_superuser',)
            }
        ),
        (_('Important dates'), {'fields': ('last_login','date_joined',)})
    )

    add_fieldsets = (
        (None,{
            'classes':('wide',),
            'fields':('email','password1','password2',),
        }),
    )


admin.site.register(User,CustomUserAdmin)