import re
from threading import Thread

from django.contrib.auth import get_user_model
from rest_framework import serializers

from api.account.exceptions import UserNotFound, UserVerified
from api.account.models import TokenVerification
from api.account.tasks import send_register_mail

User = get_user_model()


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email','mobile_number','hospital', 'password',)
        # Make sure that the password field is never sent back to the client.
        extra_kwargs = {
            'password': {'write_only': True},
        }

    def validate(self, attrs):
        if re.match("^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$", attrs['password']) is None:
            raise serializers.ValidationError(
                'password must be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character')
        return attrs

    def create(self, validated_data):
        user = User.objects.create_account(validated_data['first_name'], validated_data['last_name'],
                                           validated_data['email'], validated_data['password'], validated_data['mobile_number'], validated_data['hospital'])
        return user

class UserReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email','mobile_number','hospital',)

class UserSerializer3(serializers.ModelSerializer):
    class Meta:
        model=User
        fields = ('id','first_name','last_name',)

class TokenVerificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = TokenVerification
        fields = ('token',)
        read_only_fields = '__all__'


class ResendEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=100)

    def create(self, validated_data):
        print('e',validated_data)
        try:
            u = User.objects.get(email=validated_data.get('email'))
            if u.is_active:
                raise UserVerified
            previous_token = TokenVerification.objects.filter(user=u).delete()
            token = TokenVerification.objects.create(user=u)
            thread = Thread(target=send_register_mail, kwargs={'user': u, 'token': token})
            thread.start()
            return {'email': 'Verification email has been resent, Please verify to continue'}
        except User.DoesNotExist:
            raise UserNotFound
